<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>halaman crud</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?php foreach ($hasil->result() as $value):?>
					<form class="form" action="<?php echo base_url('/crud/action_update/').$value->id; ?>" method="post" accept-charset="utf-8">
						<fieldset>
							<legend class="col-form-legend">add</legend>
							<p><label class="sr-only">name</label><input class="form-control" type="text" name="name" placeholder="name" value="<?php echo $value->name ?>"></p>
							<p><label class="sr-only">username</label><input class="form-control" type="text" name="username" placeholder="username" value="<?php echo $value->username?>"></p>
							<p><label class="sr-only">email</label><input class="form-control" type="text" name="email" placeholder="email" value="<?php echo $value->email ?>"></p>
							<p><label class="sr-only">pass</label><input class="form-control" type="text" name="pass" placeholder="pass" value="<?php echo $value->password ?>"></p>
							<p><input class="btn btn-block btn-info" type="submit" name="submit" value="submit"></p>
						</fieldset>
					</form>
					<?php endforeach ?>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
	</body>
</html>